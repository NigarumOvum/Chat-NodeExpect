# Chat App 

## Description:
> This is a basic chat app made by NodeJS. It uses a real-time bi-directional event-based communication using [Socket.io](https://www.npmjs.com/package/socket.io). The app can be simultaneously used within different devices as long as it remains with the same chat room.

[View it live from your browser.](https://chat-nodeexpect.herokuapp.com/)<br>

  > Built with:
  * [ExpressJS](https://github.com/expressjs/express). 
  * [Moment](https://www.npmjs.com/package/moment). 
  * [Socket.io](https://www.npmjs.com/package/socket.io). 
  * [Nodemon](https://www.npmjs.com/package/nodemon). 
  * [Expect](https://github.com/mjackson/expect). 


> Things I learned in this project;
  * Set-up an basic express server.
  * How to make use a real-time bi-directional JS library called [Socket.io](https://www.npmjs.com/package/socket.io).
  * How to listen to events when users create or make chats inside the same channel.
  * Abstract time using momentjs. 
  * Validate the output data if it matches the expected data type using Expect.


# About me:

- 🤔 I’m looking for more oportunities, projects and startups to growth my skills and achieve more and more experience in many different technologies. ![developer](https://img.icons8.com/external-flat-juicy-fish/24/000000/external-developer-devops-flat-flat-juicy-fish-2.png)
- ⚡ Fun facts: I really love play video games ![videog](https://img.icons8.com/color/24/000000/controller.png), play the guitar/sing ![guitar](https://img.icons8.com/external-vitaliy-gorbachev-flat-vitaly-gorbachev/24/000000/external-guitar-camping-vitaliy-gorbachev-flat-vitaly-gorbachev.png), learn about science ![science](https://img.icons8.com/cute-clipart/24/000000/biotech.png), go to the beach ![beach](https://img.icons8.com/fluency/24/000000/beach.png), learn new technological trends very often.
- I love the coffee! ![cofee](https://img.icons8.com/external-flat-juicy-fish/24/000000/external-developer-web-developer-flat-flat-juicy-fish-2.png)
-  Portfolio Website:<a href="https://brealy-padron-portfolio-react.vercel.app//"> Portfolio </a> 
- ![gh](https://img.icons8.com/cute-clipart/24/000000/github.png) Profile: <a href="https://github.com/nigarumovum/nigarumovum"> Brealy Padron GH Profile </a>
- ![spotify](https://img.icons8.com/fluency/24/000000/spotify.png) Profile: <a href="https://open.spotify.com/user/r8o2g959rb1dyp8fexucl2mbr"> Brealy Padron Spotify Profile </a>

:mailbox: Reach me out!

- neighbordevcr@gmail.com



[<img src="https://img.shields.io/badge/LinkedIn-0077B5?style=for-the-badge&logo=linkedin&logoColor=white" />](https://www.linkedin.com/in/bfpr131095/)

# Stats: 
![visitors](https://visitor-badge.glitch.me/badge?page_id=nigarumovum.nigarumovum)

